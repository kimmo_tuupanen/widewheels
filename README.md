# React + Vite + TypeScript + Express 

This React project was made as a personal project for Buutti Trainee Academy. WideWheels is a fictional bike rental company.

## Technologies used

- React 
    - react dom
    - react-hook-form
    - react-intersection-observer
    - react-router-dom
- Express
- Axios
- shadcn/ui
- Tailwind

## API endpoints

These endpoints are used to fetch bike data and/or user loggin.


### GET
- /data – to retrieve a list of bikes from `db.json` <br/>

### POST
- /data/login – for user to login or to create a new user
 <br/>

___


