import { useEffect, useState } from "react";
import dataService from "../server/dataService";
import Container from "./Container";

import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Badge } from "@/components/ui/badge";
import { Button } from "@/components/ui/button";

interface Bike {
  id: string;
  quantity: number;
  size: string[];
  brand: string;
  model: string;
  ebike: boolean;
  image: string;
  description: string;
}

function Bikes() {
  const [bikes, setBikes] = useState<Bike[]>([]);

  useEffect(() => {
    dataService.getAll().then((data) => setBikes(data.bikes));
  }, []);

  return (
    <Container className="overflow-hidden mt-10">
      <section id="bikes" className="grid grid-cols-3 gap-8 mt-24 mb-10">
        {bikes.map((bike) => (
          <Card
            key={bike.id}
            className="flex flex-col h-full drop-shadow-[8px_8px_12px_rgba(0,0,0,0.05)]"
          >
            <CardHeader className="flex flex-col gap-4">
              <div className="h-5/6">
                <div>
                  <img
                    src={`img/${bike.image}`}
                    alt="Bike image"
                    className="object-cover block w-full hover:transform hover:scale-105 transition duration-300 mb-8"
                  />
                </div>
                <CardTitle className="mb-1.5">{bike.brand}</CardTitle>
                <CardDescription>{bike.model}</CardDescription>
              </div>
            </CardHeader>
            <CardContent className="flex-grow">
              <p>{bike.description}</p>
            </CardContent>
            <CardFooter className="flex justify-between">
              <Button variant="outline">View Details</Button>
              {bike.ebike && <Badge variant="secondary">e-Bike</Badge>}
            </CardFooter>
          </Card>
        ))}
      </section>
    </Container>
  );
}

export default Bikes;
