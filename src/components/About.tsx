import Container from "./Container";

function About() {
  return (
    <Container className="overflow-hidden">
      <section id="about" className="h-dvh mt-36">
        This is About section.
      </section>
    </Container>
  );
}

export default About;
