import { useAuth } from "../context/AuthProvider";
import Hero from "./Hero";
import Bikes from "./Bikes";
import About from "./About";
import Login from "./Login";
import Rent from "./Rent";

type MainProps = {
  inView: boolean;
  ioref: React.Ref<HTMLDivElement>;
};

function Main({ inView, ioref }: MainProps) {
  const auth = useAuth();

  return (
    <main className={`${inView ? "mt-0" : "mt-24"}`}>
      <Hero ioref={ioref} />
      <About />
      <Bikes />
      {auth.user ? <Rent /> : <Login />}
    </main>
  );
}

export default Main;
