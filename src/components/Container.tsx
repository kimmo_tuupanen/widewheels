import classNames from "classnames";
import { ReactNode } from "react";

type ContainerProps = {
  children: ReactNode;
  className: string;
};

function Container({ children, className }: ContainerProps) {
  return (
    <div
      className={classNames("mx-auto max-w-[80rem] px-8 sm:px-13", className)}
    >
      {children}
    </div>
  );
}

export default Container;
