import classNames from "classnames";
import { useAuth } from "../context/AuthProvider";
import { Link } from "react-scroll";
import { Button } from "./ui/button";

import { HiOutlineLogout } from "react-icons/hi";

type HeaderProps = {
  inView: boolean;
};

function Header({ inView }: HeaderProps) {
  const auth = useAuth();

  return (
    <>
      <header
        className={classNames(
          "flex justify-between items-center z-10 bg-[#f4f4f4] bg bg-opacity-100 h-24 transition-all px-12 border-b",
          !inView && "fixed top-0 w-full backdrop-blur-[12px] bg-opacity-85 "
        )}
      >
        <Link
          activeClass="active"
          smooth
          spy={true}
          to="home"
          className="flex items-center gap-6 opacity-80 cursor-pointer"
        >
          <img
            className="h-16"
            src="./img/logo-wheel.png"
            alt="WideWheels logo"
          />
          <img className="h-10" src="./img/logo.png" alt="WideWheels logo" />
        </Link>
        <nav className="main-nav">
          <ul
            className={classNames(
              "list-none flex items-center gap-12 no-underline font-medium text-base cursor-pointer",
              inView ? "text-[#F6F6F4]" : "text-[#748B6F]"
            )}
          >
            <li className="hover:text-[#2A403D] transition-all">
              <Link activeClass="active" offset={-140} smooth spy to="about">
                About
              </Link>
            </li>
            <li className="hover:text-[#2A403D] transition-all">
              <Link activeClass="active" offset={-140} smooth spy to="bikes">
                Rental Bikes
              </Link>
            </li>
            <li className="hover:text-[#2A403D] transition-all">
              <Link activeClass="active" smooth spy to="info">
                Info
              </Link>
            </li>
            {auth.isLoggedIn && (
              <li>
                <Button
                  className="text-md cursor-pointer bg-transparent"
                  variant={inView ? "outline" : "ghost"}
                  onClick={() => auth.logOut()}
                >
                  <p className="mr-1">LogOut</p>
                  <HiOutlineLogout />
                </Button>
              </li>
            )}
            <li>
              <Link offset={-120} smooth spy to="login">
                <Button
                  className="text-md cursor-pointer"
                  variant={inView ? "secondary" : "default"}
                >
                  Hit the Trails!
                </Button>
              </Link>
            </li>
          </ul>
        </nav>
      </header>
    </>
  );
}

export default Header;
