interface HeaderProps {
  ioref: React.Ref<HTMLDivElement>;
}

function Hero({ ioref }: HeaderProps) {
  return (
    <div
      id="home"
      ref={ioref}
      className="bg-slate-200 h-dvh grid grid-rows-3 bg-[linear-gradient(to_right_bottom,rgba(116,139,111,0.3),rgba(116,139,111,0.1)),url('./img/heroblackandwhite.jpg')] bg-cover -mt-24"
    >
      <div className="text-[#F6F6F4] row-start-2 ml-14 p-8 h-fit w-3/5 bg-[rgb(116,139,111,0.5)] rounded-lg border border-[rgb(246,246,244,0.5)] drop-shadow-xl [text-shadow:_0_1px_0_rgb(0_0_0_/_80%)]">
        <h1 className="text-4xl mb-6">Welcome to WideWheels!</h1>
        <p className="mb-4 text-lg leading-6">
          At WideWheels, we're not just about renting bikes - we're about
          unlocking unforgettable journeys. Our bike rental service offers you
          the perfect way to immerse yourself in nature's splendor. We're your
          local guides to hidden gems and undiscovered trails. Our team of
          biking enthusiasts is here to share insider tips, recommend scenic
          routes, and ensure that your adventure with us is nothing short of
          extraordinary.
        </p>
        <p className="mb-4 text-lg leading-6">
          So, pack your sense of adventure and get ready to explore trails like
          never before. Join us at WideWheels, where every journey begins with a
          pedal and ends with memories to last a lifetime.
        </p>
      </div>
    </div>
  );
}

export default Hero;
