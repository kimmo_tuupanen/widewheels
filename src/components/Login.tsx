import Container from "./Container";
import LoginForm from "./LoginForm";

function Login() {
  return (
    <Container className="overflow-hidden my-12">
      <section id="login" className="grid grid-cols-3 border rounded-lg p-6">
        <div className="border rounded-lg col-span-2 p-12 text-[#F6F6F4] bg-[linear-gradient(to_right_bottom,rgba(116,139,111,0.7),rgba(116,139,111,0.2)),url('./img/bythepond.jpg')] bg-cover opacity-90 [text-shadow:_0_1px_0_rgb(0_0_0_/_60%)]">
          <h2 className="text-3xl mb-2">
            Log in and reserve bike of your choice
          </h2>
          <p className="text-lg mb-8 leading-6">
            Whether you're a seasoned cyclist seeking adrenaline-pumping trails
            or a leisurely explorer craving tranquil rides, WideWheels has
            something for everyone. Our fleet boasts a diverse range of bikes,
            from rugged fat bikes to sleek electronic cruisers, ensuring that
            every pedal stroke is met with comfort and reliability. You can find
            all the bikes available on our shop from above. The rental fee is
            payed upon arrival.
          </p>
          <h2 className="text-3xl mb-2">Rent conditions</h2>
          <p className="text-lg leading-6">
            When renting a bike, you must leave a valid identification or a
            security deposit. The bike has to be returned in the condition as it
            was when it handed to you. The person who rents the bike is fully
            responsible for the bike the whole time of the rent until the bike
            has been handed back to WideWheels. The one who rents the bike is
            also obligated to compensate all damages and missing parts of the
            bike and of the equipment.
          </p>
        </div>
        <LoginForm />
      </section>
    </Container>
  );
}

export default Login;
