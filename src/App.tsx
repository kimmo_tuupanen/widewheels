import { useInView } from "react-intersection-observer";
import AuthProvider from "./context/AuthProvider";

import Header from "./components/Header";
import Main from "./components/Main";
import "./App.css";

function App() {
  const { ref, inView } = useInView({
    threshold: 0,
  });

  return (
    <AuthProvider>
      <Header inView={inView} />
      <Main ioref={ref} inView={inView} />
    </AuthProvider>
  );
}

export default App;
