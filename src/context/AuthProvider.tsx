import { v4 as uuidv4 } from "uuid";
import { createContext, useState, ReactNode, useContext } from "react";

import userService from "@/server/userService";

interface AuthContextType {
  token: string;
  user: string | null;
  isLoggedIn: boolean;
  loginAction: (data: {
    username: string;
    email: string;
    password: string;
  }) => void;
  logOut: () => void;
}

interface LoginActionProps {
  username: string;
  email: string;
  password: string;
}

const defaultValue: AuthContextType = {
  token: "",
  user: "",
  isLoggedIn: false,
  loginAction: () => {},
  logOut: () => {},
};

const AuthContext = createContext(defaultValue);

const AuthProvider = ({ children }: { children: ReactNode }) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [user, setUser] = useState(null);
  const [token, setToken] = useState(localStorage.getItem("widewheels") || "");

  const loginAction = async (data: LoginActionProps) => {
    const newUser = { ...data, id: uuidv4() };

    userService.createUser({ newUser: newUser }).then((returnedData) => {
      setUser(returnedData.user);
      setToken(returnedData.token);
      setIsLoggedIn(true);
      localStorage.setItem("widewheels", returnedData.token);
      return;
    });
  };

  const logOut = () => {
    setUser(null);
    setToken("");
    setIsLoggedIn(false);
    localStorage.removeItem("widewheels");
  };

  return (
    <AuthContext.Provider
      value={{ token, user, isLoggedIn, loginAction, logOut }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  return useContext(AuthContext);
};

export default AuthProvider;
