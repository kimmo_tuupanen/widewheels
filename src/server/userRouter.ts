import express from "express";
import { promisify } from "util";
import argon2 from "argon2";
import jwt from "jsonwebtoken";
import fs from "fs";
import path from "path";

const router = express.Router();
const dataPath = path.join("data", "db.json");
const secret = import.meta.env.VITE_SECRET_KEY;

const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);

router.post("/login", async (req, res) => {
  try {
    const data = await readFileAsync(dataPath, "utf-8");
    const { username, email, password, id } = req.body;
    const dataArray = JSON.parse(data);
    let user = dataArray.users.find(
      (user: { email: string }) => user.email === email
    );

    if (user) {
      const verified = await argon2.verify(user.hashedPassword, password);
      if (!verified) {
        return res.status(401).json({ error: "Incorrect password" });
      }
    } else {
      const hashedPassword = await argon2.hash(password);
      user = { id, username, email, hashedPassword: hashedPassword };

      dataArray.users.push(user);
    }

    const token = jwt.sign({ email }, secret, {
      expiresIn: "1h",
    });

    await writeFileAsync(dataPath, JSON.stringify(dataArray, null, 2));
    res.status(201).json({ user, token });
  } catch (error) {
    console.error("Error processing registration:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

export default router;
