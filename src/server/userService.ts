import axios from "axios";

const baseUrl = import.meta.env.VITE_BASEURL;

interface Props {
  newUser: {
    id?: string;
    username: string;
    email: string;
    password: string;
    token?: string;
  };
}

const getUser = async () => {
  const request = axios.get(`${baseUrl}/register`);
  const res = await request;
  return res.data;
};

const createUser = async ({ newUser }: Props) => {
  const request = axios.post(`${baseUrl}/login`, newUser);
  const res = await request;
  return res.data;
};

export default { getUser, createUser };
