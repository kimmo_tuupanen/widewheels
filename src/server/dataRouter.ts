import express from "express";
import fs from "fs";
import path from "path";

const router = express.Router();
const dataPath = path.join("data", "db.json");

//** GET */
router.get("/", (_, res) => {
  fs.readFile(dataPath, "utf-8", (err, data) => {
    if (err) {
      res.status(500).send("Error reading the data file");
      return;
    }
    res.json(JSON.parse(data));
  });
});

//** POST */
router.post("/", (req, res) => {
  fs.readFile(dataPath, "utf-8", (err, data) => {
    if (err) {
      res.status(500).send("Error reading the data file");
      return;
    }

    const notes = JSON.parse(data);
    const newNote = req.body;
    notes.push(newNote);

    fs.writeFile(dataPath, JSON.stringify(notes, null, 2), (writeError) => {
      if (writeError) {
        console.error("Error writing the data file:", writeError);
        return res.status(500).json({ error: "Error writing the data file" });
      }
      res.status(201).json(newNote);
    });
  });
});

//** PUT *//
router.put("/:id", (req, res) => {
  const { id } = req.params;
  const updatedData = req.body;

  fs.readFile(dataPath, "utf-8", (err, data) => {
    if (err) {
      res.status(500).send("Error reading the data file");
      return;
    }

    const notes = JSON.parse(data);
    const noteIndex = notes.findIndex((note: { id: string }) => note.id === id);

    if (noteIndex === -1) {
      return res.status(404).json({ message: "Note not found" });
    }

    notes[noteIndex] = { ...notes[noteIndex], ...updatedData };

    fs.writeFile(dataPath, JSON.stringify(notes, null, 2), (writeError) => {
      if (writeError) {
        console.error("Error writing the data, file:", writeError);
        return res.status(500).json({ error: "Error writing the data file" });
      }
      res.status(200).json(notes[noteIndex]);
    });
  });
});

//** DELETE */
router.delete("/:id", (req, res) => {
  const { id } = req.params;
  fs.readFile(dataPath, "utf-8", (err, data) => {
    if (err) {
      res.status(500).send("Error reading the data file");
      return;
    }

    const notes = JSON.parse(data);
    const noteIndex = notes.findIndex((note: { id: string }) => note.id === id);

    if (noteIndex === -1) {
      return res.status(404).json({ message: "Note not found" });
    }

    notes.splice(noteIndex, 1);

    fs.writeFile(dataPath, JSON.stringify(notes, null, 2), (writeError) => {
      if (writeError) {
        console.error("Error writing the data, file:", writeError);
        return res.status(500).json({ error: "Error writing the data file" });
      }
      res.status(200).json(notes);
    });
  });
});

export default router;
