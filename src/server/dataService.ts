import axios from "axios";

const baseUrl = import.meta.env.VITE_BASEURL;

interface Props {
  newUser: {
    username: string;
    email: string;
    password: string;
  };
}

const getAll = async () => {
  const request = axios.get(baseUrl);
  const res = await request;
  return res.data;
};

const create = async ({ newUser }: Props) => {
  const request = axios.post(baseUrl, newUser);
  const res = await request;
  return res.data;
};

// const update = async ({ id, newObject }: Props) => {
//   const request = axios.put(`${baseUrl}/${id}`, newObject);
//   const res = await request;
//   return res.data;
// };

// const deleteItem = async ({ id }: Props) => {
//   const request = axios.delete(`${baseUrl}/${id}`);
//   const res = await request;
//   return res.data;
// };

export default { getAll, create };
