import express from "express";
import dataRouter from "./dataRouter";
import userRouter from "./userRouter";

export const app = express();
app.use(express.json());
app.use("/data", dataRouter);
app.use("/data", userRouter);

if (!process.env["VITE"]) {
  const frontendFiles = process.cwd() + "/dist";
  app.use(express.static(frontendFiles));
  app.get("/*", (_, res) => {
    res.send(frontendFiles + "/index.html");
  });
  app.listen(process.env["PORT"]);
}
